Credits:

	Demo Images:
		Unsplash (unsplash.com)

	Icons:
		Font Awesome (fortawesome.github.com/Font-Awesome)

	Other:
		jQuery (jquery.com)
		html5shiv.js (@afarkas @jdalton @jon_neal @rem)v
		background-size polyfill (github.com/louisremi)
		Repond.js (j.mp/respondjs)
		Skel (skel.io)
		
		Modified template from html5up.net | @ajlkn
		Using From Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)